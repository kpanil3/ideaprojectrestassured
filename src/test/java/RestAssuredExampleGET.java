import static io.restassured.RestAssured.*;

import PoJoClasses.EmpInfo;
import com.fasterxml.jackson.databind.util.JSONPObject;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.core.Is;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RestAssuredExampleGET {

    @Test
    public void test1(){

       given().when().get("https://reqres.in/api/users?page=2").
                then().assertThat().statusCode(200);

    }

    @Test
    public void test2(){
        Response response = given().when().get("https://reqres.in/api/users?page=2");
        Assert.assertEquals(response.getBody().path("data[0].email"), "michael.lawson@reqres.in");

    }

    @Test
    public void test3(){
        Response response = given().when().get("https://reqres.in/api/users?page=2");
        System.out.println(response.getTimeIn(TimeUnit.MILLISECONDS));

    }

    @Test
    public void test4(){

        baseURI = "https://reqres.in";
        RequestSpecification req = given();
        Response rep = req.request(Method.GET, "/api/users?page=2");
        System.out.println(rep.statusCode());

    }

    @Test
    public void test5(){
        EmpInfo emp = new EmpInfo("KP", "Test");
        given().contentType(ContentType.JSON).when().body(emp).post("https://reqres.in/api/users").
                then().log().body().assertThat().statusCode(201);

    }

    @Test
    public void test6(){


        JSONObject json = new JSONObject();
        json.put("name", "KP1");
        json.put("job", "Test1");

        given().contentType(ContentType.JSON).when().body(json.toJSONString()).post("https://reqres.in/api/users").then().
                log().body().assertThat().statusCode(201);


    }
}
