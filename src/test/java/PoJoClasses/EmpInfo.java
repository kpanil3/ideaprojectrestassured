package PoJoClasses;

public class EmpInfo {

    public EmpInfo(String name, String job){
        this.name = name ;
        this.job = job;
    }
    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name ;
    private String job ;

}
